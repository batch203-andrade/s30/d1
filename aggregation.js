// MongoDB Aggregation
/* 
    To generate, manipulate, and perform operations to create filtered results 
    that helps us to analyze the data.
*/

// Aggregate Method
/* SYNTAX 
    db.collectionName.aggregate([
        {$match: {field:value}}
        {$group: {_id: "$value", fieldResult: "value result"}}
        { $project: { _id: 0 } } or { $sort: { _id: 0 } }
    ])
*/

// project and group
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
    { $project: { _id: 0 } }
]);

// sort -1 descending 1 ascending
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
    { $sort: { total: 0 } }
]);

// Aggregating results based on array fields
// $unwind deconstructs an array
db.fruits.aggregate([
    { $unwind: "$origin" },
    { $group: { _id: "$origin", kinds: { $sum: 1 } } }
]);

// Other aggregate stages
// $count operator all yellow fruits 
db.fruits.aggregate([
    { $match: { color: "Yellow" } },
    { $count: "Yellow Fruits" }
])

// $avg gets the average value of the stock
db.fruits.aggregate([
    { $match: { color: "Yellow" } },
    { $group: { _id: "$color", yellow_fruits_stock: { $avg: "$stock" } } }
])

// $min $max 
db.fruits.aggregate([
    { $match: { color: "Yellow" } },
    { $group: { _id: "$color", yellow_fruits_stock: { $min: "$stock" } } }
])

db.fruits.aggregate([
    { $match: { color: "Yellow" } },
    { $group: { _id: "$color", yellow_fruits_stock: { $max: "$stock" } } }
])